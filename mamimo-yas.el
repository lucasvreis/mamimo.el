;;; mamimo-yas.el --- Yasnippet defaults for mamimo -*- lexical-binding: t; -*-

;; Copyright (C) 2021-2021 Lucas Viana Reis


;;; Code:
(require 'yasnippet)

(defun mamimo-yas--activate ()
  "Activate math snippets in the current buffer using yasnippet."
  (yas-load-directory (concat mamimo--directory "snippets"))
  (yas-activate-extra-mode 'mamimo-mode)

  ;; workaround to yasnippet's issue #1110 (make keybindings work)
  (setq-default yas--direct-mamimo-mode nil)
  (setq-local yas--direct-mamimo-mode t)

  ;; cooperate with abbrev
  (defadvice! mamimo-yas--expand-before-next-field (&rest r)
    :before #'yas-next-field
    (mamimo--try-expand-abbrev)))

(defun mamimo-yas--deactivate ()
  "Deactivate mamimo-yas."
  (yas-deactivate-extra-mode 'mamimo-mode)
  (undefadvice! mamimo-yas--expand-before-next-field (&rest r)
    :before #'yas-next-field))

(define-minor-mode mamimo-yas-mode
  "Yasnippet defaults for `mamimo-mode'"
  :init-value nil
  :lighter nil
  (if mamimo-yas-mode
      (mamimo-yas--activate)
    (mamimo-yas--deactivate)))

(provide 'mamimo-yas)

;;; mamimo-yas.el ends here
