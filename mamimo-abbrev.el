;;; mamimo-abbrev.el --- Abbrev-mode defaults for mamimo -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Lucas Viana Reis

;; Author: Lucas Viana Reis <lucasvianareis@gmail.com>

;;; Code:

(defvar mamimo-default-text-abbrevs
  '(("pa" "podemos assumir")
    ("pd" "por definição")
    ("ie" "i.e.")
    ("tq" "tal que")
    ("spg" "sem perda de generalidade")
    ("qtp" "q.t.p.")
    ("sss" "se, e somente se,"))
  "List of default abbrevs for `mamimo-mode' when outside math blocks.
You can also add other text abbrevs with `edit-abbrevs' in the
table `mamimo-text-abbrev-table'.")

(defvar mamimo-literal-abbrevs '(f g h i j k l m n p q r s t u v w x y z)
  "Symbols for characters (or words) that will be literally expanded but inside math fences.")

(defvar mamimo-default-math-abbrevs '()
  "List of default abbrevs for `mamimo-mode' when inside math blocks.
You can also add other math abbrevs with `edit-abbrevs' in the
table `mamimo-math-abbrev-table'.")

(defvar mamimo-greek-abbrevs t
  "If `t', a table of greek letters abbrevs will be generated. The
abbrevs follow the bindings listed in `mamimo-greek-table', e.g. to
insert `\varphi' one enters `@vf' where `@' is the
`mamimo-greek-abbrevs-prefix'.")

(defvar mamimo-greek-abbrevs-prefix ";"
  "Prefix for the auto-generated greek letters abbrevs.
Must only contain characters from the w (word) syntax category.")

(defvar mamimo-greek-table
  '((a . alpha)
    (b . beta)
    (d . delta)
    (D . Delta)
    (e . epsilon)
    (et . eta)
    (f . phi)
    (F . Phi)
    (g . gamma)
    (G . Gamma)
    (h . eta)
    (i . iota)
    (k . kappa)
    (l . lambda)
    (L . Lambda)
    (m . mu)
    (n . nu)
    (o . theta)
    (O . Theta)
    (om . omega)
    (Om . Omega)
    (p . pi)
    (P . Pi)
    (ps . psi)
    (Ps . Psi)
    (ph . phi)
    (Ph . Phi)
    (r . rho)
    (s . sigma)
    (S . Sigma)
    (t . tau)
    (th . theta)
    (Th . Theta)
    (u . upsilon)
    (U . Upsilon)
    (ve . varepsilon)
    (vf . varphi)
    (vt . vartheta)
    (vp . varpi)
    (vr . varrho)
    (vs . varsigma)
    (w . omega)
    (W . Omega)
    (x . xi)
    (X . Xi)
    (y . iota)
    (z . zeta)))

(defun mamimo--generate-literal-abbrevs ()
  (cl-loop for s in mamimo-literal-abbrevs
           collect `(,(symbol-name s) ,(format "\\(%s\\)" s) nil :system t)))

(defun mamimo--generate-t-greek-abbrevs ()
  (cl-loop for (a . l) in mamimo-greek-table
           with p = mamimo-greek-abbrevs-prefix
           collect  `(,(concat p (symbol-name a)) ,(format "\\(\\%s\\)" l) nil :case-fixed t :system t)))

(defun mamimo--generate-m-greek-abbrevs ()
  (cl-loop for (a . l) in mamimo-greek-table
           with p = mamimo-greek-abbrevs-prefix
           collect  `(,(concat p (symbol-name a)) ,(format "\\%s" l) nil :case-fixed t :system t)))

(defun mamimo-abbrev--deactivate ()
  (setq mamimo-text-abbrev-table nil)
  (setq mamimo-math-abbrev-table nil)
  (setq abbrev-minor-mode-table-alist (assoc-delete-all 'mamimo-mode abbrev-minor-mode-table-alist)))

(defun mamimo-abbrev--activate ()
  (mamimo-abbrev--deactivate)
  (let ((temp-syntax-table (make-syntax-table (syntax-table))))
      (modify-syntax-entry 59 "w" temp-syntax-table)
      (defadvice! mamimo-custom-abbrev-syntax (f &rest r)
        :around #'define-mode-abbrev
        :around #'abbrev--before-point
        (with-syntax-table temp-syntax-table
          (apply f r)))

    (define-abbrev-table
      'mamimo-text-abbrev-table
      (append
       (mapcar (lambda (l) (append l '(nil :system t))) mamimo-default-text-abbrevs)
       (mamimo--generate-literal-abbrevs)
       (mamimo--generate-t-greek-abbrevs))
      :enable-function #'mamimo-notmathp)
    (define-abbrev-table
      'mamimo-math-abbrev-table
      (append
        (mapcar (lambda (l) (append l '(nil :system t))) mamimo-default-math-abbrevs)
        (mamimo--generate-m-greek-abbrevs))
      :enable-function #'mamimo-mathp)

    (push `(mamimo-mode . (,mamimo-math-abbrev-table
                           ,mamimo-text-abbrev-table))
          abbrev-minor-mode-table-alist)
    (abbrev-mode +1)))

(defun mamimo--try-expand-abbrev ()
  (when (eq (char-syntax (char-before)) ?w)
    (expand-abbrev)))

(defun mamimo-expand-abbrev-or-default ()
  (interactive)
  (unless (mamimo--try-expand-abbrev)
    (let ((mamimo-abbrev-mode nil))
      (if-let* ((binding (or (key-binding (kbd "<tab>"))
                             (key-binding (kbd "TAB")))))
          (call-interactively binding)))))

(define-minor-mode mamimo-abbrev-mode
  "Abbrev defaults for `mamimo-mode'"
  :init-value nil
  :lighter nil
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "<tab>") 'mamimo-expand-abbrev-or-default)
            map)
  (if mamimo-abbrev-mode
      (mamimo-abbrev--activate)
    (mamimo-abbrev--deactivate)))

(provide 'mamimo-abbrev)

;;; mamimo-abbrev.el ends here
