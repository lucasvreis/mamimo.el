;;; mamimo.el --- Unified configuration for major modes supporting mathematics  -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021-2021 Lucas Viana Reis
;;
;; Author: Lucas Viana Reis <l240191@dac.unicamp.br>
;; Modified: 16 Nov 2021
;; Version: 0.1
;; Keywords: abbrev, convenience, tex
;; Homepage: https://gitlab.com/lucasvreis/mamimo.el
;; Package-Requires: ((emacs "26.2") (yasnippet "0") (aas "0"))
;; SPDX-License-Identifier: GPL-3.0-or-later
;;
;; This file is not part of GNU Emacs.

;;; Commentary:
;;
;; Package providing unified tools for modes supporting (tex) mathematics.
;;
;; For example (all can be disabled individually):
;;  - A hook that is run in all math modes
;;  - Two hooks that run when entering or exiting math environments
;;  - Use one yasnippet folder for math snippets shared across all math modes
;;  - Disable input-method while writing equations, and restore it afterwards
;;  - Super fast and more "natural" LaTeX mathematics insertion
;;  - Leverages prettify-symbols-mode for increased LaTeX legibility
;;  - Smart spacing around infixes, but not when you may not want it
;;  - In many cases, takes care of \left and \right for you
;;  - Edit matrices and arrays as org tables (in progress)
;;  - Use abbrev-mode magics and shenaningans
;;  - Org mode specific features:
;;    + Advices for LaTeX native fontification that makes it display
;;      vertically shifted subscripts and superscripts.
;;
;; This library is heavily inspired by `LaTeX-auto-activating-snippets' (`laas')
;; and `castel.dev''s Vim configuration. But unlike `laas', MaMiMo is more aimed
;; at providing a framework with smart and precise tools rather than a set of
;; predefined snippets. It integrates features like `yasnippet', `abbrev-mode'
;; and `aas' for that.
;;

;;; Code:

(defconst mamimo--directory (file-name-directory load-file-name))

(require 'mamimo-mathp)
(require 'mamimo-yas)
(require 'mamimo-prettify)
(require 'mamimo-aas)
(require 'mamimo-abbrev)
(require 'mamimo-smartkeys)


;;; Math block hooks

(defvar mamimo-enter-math-block-hook nil
  "Hook which is run when the cursor enters a math block.")

(defvar mamimo-exit-math-block-hook nil
  "Hook which is run when the cursor exits a math block.")

(defun mamimo--run-math-block-hooks ()
  (if (and (boundp 'mamimo--math-block-p) (xor mamimo--math-block-p (mamimo-mathp)))
      (if mamimo--math-block-p
          (progn
            (setq mamimo--math-block-p nil)
            (run-hooks 'exit-math-block-hook))
        (setq mamimo--math-block-p t)
        (run-hooks 'enter-math-block-hook))))


;;; Input method

(defvar mamimo-deactivate-input-method-inside-math t
  "When t, deactivate the input-method when cursor is inside math blocks.
If you are using the Emacs input method, this means that you
don't need to press space when insering symbols like `^' or `~'.
Note that in general, the so-called dead keys (keys like `~') are
handled by the OS, and in this case there is no way for Emacs to
deactivate the behaviour. To use this feature you must choose a
input method with no dead keys in the OS (some will allow to
modify this setting per-application), and instead use the Emacs
builtin input method when necessary.")

(defun mamimo-setup-input-method ()
  (defun mamimo--input-method-off ()
    (setq-local previous-input-method
                (or (bound-and-true-p evil-input-method) current-input-method))
    (if (featurep 'evil)
        (evil-deactivate-input-method))
    (deactivate-input-method))
  (defun mamimo--input-method-on ()
    (activate-input-method (bound-and-true-p previous-input-method)))
  (add-hook 'enter-math-block-hook #'mamimo--input-method-off 0 t)
  (add-hook 'exit-math-block-hook #'mamimo--input-method-on 0 t))


;;; Prettify
(defvar mamimo-enable-prettify t
  "TODO")

;;; Auto-expanding snippets
(defvar mamimo-enable-aas t
  "TODO")



;;; Minor mode definition

(defun mamimo-set-defaults ()
  (interactive)
  ;; (when mamimo-deactivate-input-method-inside-math
  ;;   (mamimo-setup-input-method))
  (when mamimo-enable-prettify
    (require 'mamimo-prettify))
  ;;   (mamimo-setup-prettify))
  (when mamimo-enable-aas
    (require 'mamimo-aas)
    (mamimo-setup-aas)))
  ;; (setq font-latex-script-display '((raise -0.3) . (raise 0.4))
  ;;       font-latex-fontify-script 'multi-level)

(defun mamimo--init-h ()
  (setq-local mamimo--math-block-p nil) ;; Math block hooks
  (add-hook 'post-command-hook #'mamimo--run-math-block-hooks)
  (turn-off-smartparens-mode)
  (turn-on-show-smartparens-mode))

(define-minor-mode mamimo-mode
  "Minor mode for mathematics"
  :init-value nil
  :after-hook (mamimo-set-defaults)
  (setq font-lock-extra-managed-props '(display))
  (font-lock-flush)
  (mamimo--init-h)
  (let ((sign (if mamimo-mode 1 -1)))
    (mamimo-yas-mode sign)
    (mamimo-prettify-mode sign)
    (mamimo-abbrev-mode sign)
    (mamimo-smartkeys-mode sign)))

(provide 'mamimo)

;;; mamimo.el ends here
