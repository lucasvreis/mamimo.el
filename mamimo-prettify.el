;;; mamimo-prettify.el -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Lucas Viana Reis

;; Author: Lucas Viana Reis <lucasvianareis@gmail.com>

(defun mamimo--prettify-symbols-compose-p (_start end _match)
  (and
   (or
    ;; Allow for math delimiters
    (eq ?\) (char-before end))
    (eq ?\( (char-before end))
    ;; Only compose inside math
    (save-excursion
      (goto-char end)
      (mamimo-mathp)))
   (or
    ;; If the matched symbol doesn't end in a word character, then we
    ;; simply allow composition.  The symbol is probably something like
    ;; \|, \(, etc.
    (not (eq ?w (char-syntax (char-before end))))
    ;; Else we look at what follows the match in order to decide.
    (let* ((after-char (char-after end))
           (after-syntax (char-syntax after-char)))
      (not (or
            ;; Don't compose \alpha@foo.
            (eq after-char ?@)
            ;; The \alpha in \alpha2 or \alpha-\beta may be composed but
            ;; of course \alphax may not.
            (and (eq after-syntax ?w)
                 (not (memq after-char
                            '(?0 ?1 ?2 ?3 ?4 ?5 ?6 ?7 ?8 ?9 ?+ ?- ?' ?\" ?$ ?_))))
            ;; Don't compose inside verbatim blocks.
            (eq 2 (nth 7 (syntax-ppss)))))))))

(defvar-local mamimo-prettify--symbols-alist nil)
(defvar-local mamimo-prettify--previous-symbols-alist nil)

(defun mamimo-prettify--define-symbols ()
  (require 'tex-mode)
  (setq mamimo-prettify--symbols-alist
        (append
         '(("\\left" . ?ʟ)
           ("\\right" . ?ʀ)
           ("\\lvert" . ?|)
           ("\\rvert" . ?|)
           ("\\middle" . ?ᴍ)
           ("\\colon" . ?：)
           ("\\|" . ?‖)
           ("\\tilde" . ?˜)
           ("\\implies" . ?⇒)
           ("\\impliedby" . ?⇐)
           ;; ("\\{" . ?\｛)
           ;; ("\\}" . ?\｝)
           ("\\RR" . ?ℝ)
           ("\\CC" . ?ℂ)
           ("\\ZZ" . ?ℤ)
           ("\\NN" . ?ℕ)
           ("\\sqrt" . ?√)
           ("\\dots" . ?…)
           ("\\not\\subset" . ?⊄))
         (bound-and-true-p tex--prettify-symbols-alist))))


(defun mamimo-prettify--activate ()
  (mamimo-prettify--define-symbols)
  (setq-local mamimo-prettify--previous-symbols-alist prettify-symbols-alist)
  (setq-local prettify-symbols-alist mamimo-prettify--symbols-alist)
  ;; (setq-local prettify-symbols-unprettify-at-point t)
  (setq-local prettify-symbols-compose-predicate
              #'mamimo--prettify-symbols-compose-p)
  (prettify-symbols-mode +1))

(defun mamimo-prettify--deactivate ()
  (setq-local prettify-symbols-alist mamimo-prettify--previous-symbols-alist)
  (setq-local prettify-symbols-compose-predicate
              #'prettify-symbols-default-compose-p)
  (prettify-symbols-mode -1))

(define-minor-mode mamimo-prettify-mode
  "Prettification of math for `mamimo-mode'"
  :init-value nil
  :lighter nil
  (if mamimo-prettify-mode
      (mamimo-prettify--activate)
    (mamimo-prettify--deactivate)))

(provide 'mamimo-prettify)
