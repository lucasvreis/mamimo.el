(defun mamimo-string-replace (old new string)
  (if (fboundp 'string-replace)
      (string-replace old new string)
    (replace-regexp-in-string (regexp-quote old) new string t t)))

(provide 'mamimo-utils)
