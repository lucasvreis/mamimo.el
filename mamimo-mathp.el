;;; mamimo-mathp.el --- Precise mathematical environment detector for mamimo -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021-2021 Lucas Viana Reis
;;
;; Author: Lucas Viana Reis <l240191@dac.unicamp.br>
;; Modified: 16 Nov 2021
;; Keywords: tex
;; SPDX-License-Identifier: GPL-3.0-or-later
;;
;; This file is part of mamimo.el.
;;
;;; Commentary:
;; Since editing mathematics is so different from normal insertion in mamimo, we
;; must be as precise as possible when detecting weather the cursor is inside
;; LaTeX mathematics. `texmathp' is great for this, but it is not suited for Org
;; mode, when dollar signs have different interpretations depending on where
;; they appear. So here we define custom detectors for each mode.

;;; Code:

(require 'texmathp)

(defvar mamimo-mathp-alist
  '((org-mode . mamimo-orgmathp)
    (latex-mode . texmathp))
  "Alist whose keys are modes and values are functions that detect
math environments. IMPORTANT: every function here must properly
set `texmathp-why', even if it does not use `texmathp'
internally!")

(defvar mamimo-notmathp-alist
  '((org-mode . mamimo-orgnotmathp))
  "Alist whose keys are modes and values are functions that detect
math environments. IMPORTANT: every function here must properly
set `texmathp-why', even if it does not use `texmathp'
internally!")

;; TODO: handle derived modes?
(defun mamimo-mathp ()
  (funcall (alist-get major-mode mamimo-mathp-alist #'texmathp)))

(defun mamimo-notmathp ()
  (funcall (alist-get major-mode mamimo-notmathp-alist (lambda () (not (texmathp))))))

;;;###autoload
(defun mamimo-orgmathp ()
  "This is intended for use within Org mode.
This function combines `org-element' API and `texmathp' for a
 very precise detection."
  (if-let* ((context (org-element-context))
            (_ (memq (car context) '(latex-fragment latex-environment)))
            (delimiter (plist-get (cadr context) :begin))
            (min (point-min))
            (max (point-max)))
      (if (buffer-narrowed-p)
          (prog2 (narrow-to-region delimiter max) (texmathp) (narrow-to-region min max))
        (prog2 (narrow-to-region delimiter max) (texmathp) (widen)))))

;;;###autoload
(defun mamimo-orgnotmathp ()
  "This is intended for use within Org mode.
This function combines `org-element' API and `texmathp' for a
 very precise detection."
  (when (memq (car (org-element-at-point-no-context)) '(paragraph headline))
    (if-let* ((context (org-element-context))
              (_ (memq (car context) '(latex-fragment latex-environment)))
              (delimiter (plist-get (cadr context) :begin))
              (min (point-min))
              (max (point-max)))
        (if (buffer-narrowed-p)
            (prog2 (narrow-to-region delimiter max) (not (texmathp)) (narrow-to-region min max))
          (prog2 (narrow-to-region delimiter max) (not (texmathp)) (widen)))
      t)))

(provide 'mamimo-mathp)

;;; mamimo-mathp.el ends here
