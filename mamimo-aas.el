;;; mamimo-aas.el -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Lucas Viana Reis

;; Author: Lucas Viana Reis <lucasvianareis@gmail.com>

(require 'aas)
(require 'yasnippet)
(require 'mamimo-smartkeys)
(require 'mamimo-utils)

(defun tex-not-command-p ()
  (not (looking-back "\\\\[[:alpha:]]*?" (line-beginning-position))))
  ;; (or (not (memq (get-char-code-property (char-after) 'general-category)
  ;;                '(Ll Lu Lo Lt Lm Mn Mc Me Nl)))

(defvar mamimo-aas-text
  '(:cond (lambda () (and (tex-not-command-p) (mamimo-notmathp)))
    ";fun"  (lambda () (interactive) (yas-expand-snippet "\\\\(${1:f} \\colon ${2:A} \\to ${3:B}\\\\)$0"))))

(defvar mamimo-aas-math-infix
  '("!>"   "\\mapsto"
    "**"   "\\cdot"
    "<<"   "\\ll"
    "<="   "\\impliedby"
    "=>"   "\\implies"
    "<>"   "\\diamond"
    "le"   "\\leq"
    "ge"   "\\geq"
    "!="   "\\neq"
    "=="   "&="
    ;; "|="   "\\models"
    "=~"   "\\simeq"
    "~="   "\\cong"
    "-="   "\\equiv"
    "~~"   "\\sim"
    ">>"   "\\gg"
    "<<"   "\\ll"
    "->"   "\\to"
    "em"   "\\in"
    "nem"  "\\notin"
    "xx"   "\\times"
    "iff"  "\\iff"
    "to"   "\\to"
    "cup"  "\\cup"
    "com"  "\\circ"
    "ss"   "\\subseteq"
    "nss"  "\\not\\subseteq"
    "sps"  "\\supseteq"
    "nsps" "\\not\\superseteq"
    "sm"   "\\setminus"
    ";fa"  "\\forall"
    ";ex"  "\\exists"
    "cap"  "\\cap"))

(defun mamimo-aas--prepare-infixes ()
  (cl-loop for w in mamimo-aas-math-infix
           count t into i
           if (cl-oddp i)
           collect w
           else
           collect `(lambda nil (interactive) (mamimo-smartinfix-string ,w))))

(defun mamimo-aas--prepare-delimiters ()
  (cl-loop for d in mamimo-delimiters
           collect (car d)
           collect `(lambda nil
                      (interactive)
                      (yas-expand-snippet
                       ,(format "%s$1%s$0"
                                (mamimo-string-replace "\\" "\\\\" (car d))
                                (mamimo-string-replace "\\" "\\\\" (cdr d)))))))

(defvar mamimo-aas-math-preparations
  '(mamimo-aas--prepare-infixes
    mamimo-aas--prepare-delimiters)
  "List of functions that generate additional auto expanding
snippets for math environments.")

(defvar mamimo-aas-math
  `(:cond (lambda () (and (tex-not-command-p) (mamimo-mathp)))

    ";cas"  ,(cmd! (yas-expand-snippet "\\begin\{cases\} $1\\end\{cases\}$0"))

    "norm" ,(cmd! (yas-expand-snippet "\\lVert $1 \\rVert$0"))
    "abs" ,(cmd! (yas-expand-snippet "\\lvert $1 \\rvert$0"))
    "set" ,(cmd! (yas-expand-snippet "\\\\{ $1 \\\\}$0"))
    "fun"  ,(cmd! (yas-expand-snippet "${1:f} : ${2:A} \\to ${3:B}$0"))
    "cal"  ,(cmd! (yas-expand-snippet "\\mathcal{$1}$0"))

    "tt"   ,(cmd! (yas-expand-snippet "\\text{$1}$0"))
    "sr"   ,(cmd! (yas-expand-snippet "\\sqrt{$1}$0"))
    "fr"   ,(cmd! (mamimo-smartleftright) (yas-expand-snippet "\\frac{$1}{$2}$0"))

    "oo"   "\\infty"
    "+-"   "\\pm"
    "-+"   "\\mp"
    ".."   "\\dots"
    "c.."  "\\cdots"

    "sq"    "^2"
    "cb"    "^3"
    "inv"   "^{-1}"

    "bcap"  ,(cmd! (mamimo-smartleftright) (insert "\\bigcap"))
    "bcup"  ,(cmd! (mamimo-smartleftright) (insert "\\bigcup"))

    "lim" ,(cmd! (insert "\\lim_") (mamimo-smart-brackets-env))
    "prod" ,(cmd! (insert "\\prod_") (mamimo-smart-brackets-env))
    "sum" ,(cmd! (mamimo-smartleftright) (insert "\\sum_") (mamimo-smart-brackets-env))
    "lint" ,(cmd! (mamimo-smartleftright) (yas-expand-snippet "\\int $1 \\;d\\mu $0"))
    "slint" ,(cmd! (mamimo-smartleftright) (insert "\\int_") (yas-expand-snippet "${1:$(when (> (length yas-text) 1) \"{\")}${1:i}${1:$(when (> (length yas-text) 1) \"}\")} $2 \\;d\\mu $0"))

    "arccos" "\\arccos"
    "arccot" "\\arccot"
    "arccot" "\\arccot"
    "arccsc" "\\arccsc"
    "arcsec" "\\arcsec"
    "arcsin" "\\arcsin"
    "arctan" "\\arctan"
    "cos"    "\\cos"
    "cot"    "\\cot"
    "csc"    "\\csc"
    "exp"    "\\exp"
    "ln"     "\\ln"
    "log"    "\\log"
    "perp"   "\\perp"
    "sin"    "\\sin"
    "star"   "\\star"
    "gcd"    "\\gcd"
    "min"    "\\min"
    "max"    "\\max"
    "inf"    "\\inf"
    "sup"    "\\sup"

    "CC" "\\CC"
    "FF" "\\FF"
    "HH" "\\HH"
    "NN" "\\NN"
    "PP" "\\PP"
    "QQ" "\\QQ"
    "RR" "\\RR"
    "ZZ" "\\ZZ"))

(defun mamimo-setup-aas ()
  (apply #'aas-set-snippets 'mamimo-m (append mamimo-aas-math (mapcan #'funcall mamimo-aas-math-preparations)))
  (apply #'aas-set-snippets 'mamimo-t mamimo-aas-text)

  (add-hook 'aas-pre-snippet-expand-hook #'mamimo--try-expand-abbrev)

  (if mamimo-mode
      (progn
        (aas-mode +1)
        (aas-activate-keymap 'mamimo-t)
        (aas-activate-keymap 'mamimo-m))
    (aas-deactivate-keymap 'mamimo-t)
    (aas-deactivate-keymap 'mamimo-m)))

(provide 'mamimo-aas)
