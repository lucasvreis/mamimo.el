;;; mamimo-smartkeys.el --- Make text insertion in math environments smarter  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Lucas Viana Reis

;; Author: Lucas Viana Reis <lucasvianareis@gmail.com>
;; Keywords: extensions

(defvar mamimo-literal-infixes '(?+ ?: ?- ?= ?* ?> ?<)
  "TODO")

(defvar mamimo-infix-ignored (seq-map #'identity "{$^_<([,\\&=")
  "TODO")

(defun mamimo-smartinfix-string (str)
  (if (memq (char-before) mamimo-infix-ignored)
      (insert str)
    (unless (eq (char-before) 32)
      (insert " "))
    (insert str)
    (message "%s" (char-after))
    (unless (looking-at " +[^\\]")
      (insert " "))))


(defun mamimo-smart-brackets-env ()
  (yas-expand-snippet "${1:$(when (> (length yas-text) 1) \"{\")}${1:i}${1:$(when (> (length yas-text) 1) \"}\")}$0"))

(defun mamimo-smartinfix-key (ins)
  (if (memq (save-excursion (skip-chars-backward " ") (char-before))
            mamimo-infix-ignored)
      (call-interactively ins)
    (unless (eq (char-before) 32)
      (insert " "))
    (call-interactively ins)
    (unless (looking-at " +[^\\]")
      (insert " "))))

(defvar mamimo-delimiters
  '(("(" . ")")
    ("[" . "]")
    ("\\lvert" . "\\rvert")
    ("\\lVert" . "\\rVert")
    ("\\{" . "\\}")
    ("\\lceil" . "\\rceil"))
  "Delimiters that will be used in the following functions:
- `mamimo-aas--prepare-delimiters'
- `mamimo-smartleftright'

You are free to set equal left and right delimiters, but it may
lead to some ambiguity issues.")


(defvar mamimo--mathp-pairs
  '(("\\(" . "\\\\)")
    ("\\[" . "\\\\]"))
  "See `mamimo--find-end-of-math'.")

(defun mamimo--find-end-of-math (mathp-match)
    "This is a bit hacky. It is used for finding the matching end of
the math environment. In some situations (like nested math
environments) it may not work. \\begin \\end environments are
handled automatically, but also in a naive way."
  (or (save-excursion
        (if-let* ((limit (save-excursion (forward-line 10) (point))) ;; Ten lines limit for extra safety.
                  (matching-end (cdr (assoc mathp-match mamimo--mathp-pairs))))
            (and (re-search-forward matching-end limit t) (match-beginning 0))
          (and (re-search-forward (format "\\\\end{%s}" mathp-match) limit t) (match-beginning 0))))
      (line-end-position)))

(defvar mamimo-left-right '("\\left" . "\\right")
  "`\\left' and `\\right' -like macros that will be used in `mamimo-smartleftright'.
Change this if you want to use things like `\\mleft' and
`\\mright' from the `mleftright' latex package (it fixes some
spacing issues for you!).")

(defun mamimo-smartleftright ()
  (when (mamimo-mathp) ;; When inside math
    (let* ((mathp-match (car texmathp-why))
           (downlim (+ (length mathp-match) (cdr texmathp-why)))
           (uplim (mamimo--find-end-of-math mathp-match))
           (n (length mamimo-delimiters))
           (boths (cl-loop for p in mamimo-delimiters
                           count t into i
                           if (equal (car p) (cdr p))
                           collect (1- i))) ;; indexes of the delimiters that both close and open
           (ldelim (mapcar #'car mamimo-delimiters)) ;; all left delimiters
           (rdelim (mapcar #'cdr mamimo-delimiters)) ;; all right delimiters
           (joined (append ldelim rdelim)) ;; all delimiters
           (regex (regexp-opt joined)) ;; match any delimiter
           (left (car mamimo-left-right)) ;; left \\left
           (right (cdr mamimo-left-right)) ;; right \\right
           (ldata (make-vector n nil)) ;; left delimiters data. will be filled below
           (rdata (make-vector n nil))) ;; right delimiters data. will be filled below
      (dotimes (i n) (aset ldata i (cons 0 nil))
                     (aset rdata i (cons 0 nil))) ;; initialize data vectors with '(0) entries
      (save-excursion
        (let ((pt (point)))
          (goto-char downlim)
          (while (re-search-forward regex uplim t)
            (unless (or (equal (buffer-substring-no-properties
                                (max (point-min) (- (match-beginning 0) (length left)))
                                (match-beginning 0))
                               left)
                        (equal (buffer-substring-no-properties
                                (max (point-min) (- (match-beginning 0) (length right)))
                                (match-beginning 0))
                               right))
              (let* ((l? (<= (point) pt))
                     (m (match-string-no-properties 0))
                     (j (cl-position m joined :test #'equal))
                     (i (mod j n))
                     (closing (>= j n))
                     (both (memq i boths))
                     (el (aref (if l? ldata rdata) i)))
                (if both
                    (progn (if l? (setf (cdr el) (match-beginning 0))
                             (unless (cdr el) (setf (cdr el) (match-beginning 0))))
                           (setf (car el) (1+ (car el))))
                  (setf (car el) (if (eq l? closing) (1- (car el)) (1+ (car el))))
                  (when (eq (car el) 1)
                    (setf (car el) 0)
                    (setf (cdr el) (cons (match-beginning 0) (cdr el))))
                  (when (and l? (eq (car el) -1))
                    (setf (car el) 0)
                    (setf (cdr el) (cddr el))))))))
        (setq shift nil)
        (dotimes (i n) ;; for each pair of delimiters
          (let ((l (aref ldata i)) ;; get all left delimiter matches
                (r (aref rdata i))) ;; get all right delimiter matches
            (if (memq i boths)
                (progn (when (cl-oddp (car l))
                         (setq shift (mamimo--safeinsert shift (cdr l) left)))
                       (when (cl-oddp (car r))
                         (setq shift (mamimo--safeinsert shift (cdr r) right))))
              (dolist (pos (cdr l)) (setq shift (mamimo--safeinsert shift pos left)))
              (dolist (pos (cdr r)) (setq shift (mamimo--safeinsert shift pos right))))))))))

(defun mamimo--store-shift (storage position shift)
  (if-let* ((first (car storage)))
      (cond ((> position (car first))
             (if-let* ((rest (cdr storage)))
                 (cons first (mamimo--store-shift rest position shift))
               (list first (cons position shift))))
            ((= position (car first))
             (setf (cdr first) (+ shift (cdr first))) storage)
            ((< position (car first))
             (push (cons position shift) storage)))
    (list (cons position shift))))

(defun mamimo--get-shift (storage position &optional sum)
  (unless sum (setq sum 0))
  (if-let* ((first (car storage)))
      (cond ((> position (car first))
             (mamimo--get-shift (cdr storage) position (+ (cdr first) sum)))
            ((= position (car first))
             (+ (cdr first) sum))
            ((< position (car first))
             sum))
    sum))

(defun mamimo--safeinsert (storage pos str)
  (goto-char (+ pos (mamimo--get-shift storage pos)))
  (insert str)
  (mamimo--store-shift storage pos (length str)))

(defun mamimo-smartkeys--define-map (map)
  (dolist (key mamimo-literal-infixes)
    (define-key map (vector key)
      `(lambda ()
         (interactive)
         (let* ((mamimo-smartkeys-mode nil)
                (ins (key-binding ,(vector key))))
           (if (mamimo-mathp)
               (progn
                 (mamimo--try-expand-abbrev)
                 (mamimo-smartinfix-key ins))
             (call-interactively ins))))))
  (dolist (key '(?_ ?^))
    (define-key map (vector key)
      `(lambda ()
         (interactive)
         (mamimo--try-expand-abbrev)
         (let* ((mamimo-smartkeys-mode nil))
           (if (mamimo-notmathp)
               (call-interactively (key-binding ,(vector key)))
             (self-insert-command 1)
             (mamimo-smart-brackets-env))))))
  map)

(define-minor-mode mamimo-smartkeys-mode
  "Smart keys for `mamimo-mode' TODO"
  :init-value nil
  :lighter nil
  :keymap (mamimo-smartkeys--define-map (make-sparse-keymap)))

(provide 'mamimo-smartkeys)

;;; mamimo-smartkeys.el ends here
